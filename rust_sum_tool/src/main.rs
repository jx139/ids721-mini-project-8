use std::env;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: {} <file>", args[0]);
        std::process::exit(1);
    }

    match calculate_sum_from_file(&args[1]) {
        Ok(sum) => println!("Sum: {}", sum),
        Err(e) => eprintln!("Error: {}", e),
    }
}

fn calculate_sum_from_file(file_path: &str) -> Result<i32, io::Error> {
    let path = Path::new(file_path);
    let file = File::open(path)?;
    let buf = io::BufReader::new(file);
    let sum = buf
        .lines()
        .map_while(Result::ok) // Use map_while to stop on the first Err
        .filter_map(|line| line.parse::<i32>().ok())
        .sum();
    Ok(sum)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_calculate_sum_from_file() {
        let test_file_path = "test_data.txt";
        let expected_sum: i32 = 416; // Adjust based on your test file
        assert_eq!(
            calculate_sum_from_file(test_file_path).unwrap(),
            expected_sum
        );
    }
}
