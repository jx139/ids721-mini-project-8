# Rust Sum Tool
![pipeline status](https://gitlab.oit.duke.edu/jx139/ids721-mini-project-8/badges/main/pipeline.svg)

A simple Rust command-line tool designed to read numbers from a file, calculate their sum, and print the result. This project serves as an example of basic Rust programming, data ingestion and processing, and includes unit tests to ensure the functionality works as expected.

## Features

- **Data Ingestion**: Reads numbers from a file where each number is on a new line.
- **Data Processing**: Calculates the sum of the numbers.
- **Error Handling**: Gracefully handles errors like file not found or invalid data.
- **Unit Testing**: Includes basic unit tests to verify the tool's functionality.

## Getting Started

### Prerequisites

- Rust and Cargo: Ensure you have Rust and Cargo installed on your system. If not, you can install them by following the instructions on the [official Rust website](https://www.rust-lang.org/tools/install).

### Installation

1. **Clone the Repository**

    ```
    git clone https://yourrepository/rust_sum_tool.git
    cd rust_sum_tool
    ```

2. **Build the Project**

    Run the following command in the project directory to compile the project:

    ```
    cargo build --release
    ```

    The executable will be located in `target/release/rust_sum_tool`.

### Usage

To use the tool, run:

```
./target/release/rust_sum_tool <path_to_file>
```

Replace `<path_to_file>` with the path to the file containing the numbers you want to sum up. Each number should be on its own line in the file.

#### Example

1. Create a file named `numbers.txt` with the following content:

    ```
    2
    3
    5
    ```

2. Run the tool:

    ```
    ./target/release/rust_sum_tool numbers.txt
    ```

    Expected Output:

    ```
    Sum: 10
    ```

![numbers](./pic/numbers.png)
## Running the Tests

To run the included unit tests, execute:

```
cargo test
```

This will run all the tests included in the project, and you should see the test report as the output indicating whether the tests have passed or failed.

![test](./pic/test.png)